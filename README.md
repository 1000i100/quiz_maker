# Quiz Maker

Génère un QCM / quiz / test à partir d'un fichier de description type Markdown 

## [Générateur en ligne](https://1000i100.frama.io/quiz_maker/)

## Exemple de Quiz 
- [Quiz de démonstration](https://1000i100.frama.io/quiz_maker/demo-examples/quiz.html) et le [fichier source](https://framagit.org/1000i100/quiz_maker/raw/master/demo-examples/quiz.md) dont est issue cette démo. 
- [Quiz de démonstration : agression-fuite-inibition](https://1000i100.frama.io/quiz_maker/demo-examples/agression-fuite-inibition.html) et le [fichier source](https://framagit.org/1000i100/quiz_maker/raw/master/demo-examples/agression-fuite-inibition.txt) dont est issue cette démo. 

<iframe src="https://1000i100.frama.io/quiz_maker/demo-examples/" width="100%" height="500px"></iframe>

## Usage en ligne de commande :
```
npx quiz_maker monQuiz.md
```
