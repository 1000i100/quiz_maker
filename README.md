[![pipeline status](https://framagit.org/1000i100/quiz-maker/badges/main/pipeline.svg)](https://framagit.org/1000i100/quiz-maker/-/pipelines)
[![coverage report](https://framagit.org/1000i100/quiz-maker/badges/main/coverage.svg)](https://1000i100.frama.io/quiz-maker/coverage/)

[![duplication](https://1000i100.frama.io/quiz-maker/jscpd-badge.svg)](https://1000i100.frama.io/quiz-maker/jscpd/)
[![maintainability](https://1000i100.frama.io/quiz-maker/worst-maintainability.svg)](https://1000i100.frama.io/quiz-maker/maintainability/)

[![release](https://img.shields.io/npm/v/quiz-maker.svg)](https://www.npmjs.com/package/quiz-maker)
[![usage as download](https://img.shields.io/npm/dy/quiz-maker.svg)](https://www.npmjs.com/package/quiz-maker)

EN | [FR](README.fr.md)

# Quiz Maker

Génère un QCM / quiz / test à partir d'un fichier de description type Markdown

## [Générateur en ligne](https://1000i100.frama.io/quiz-maker/)

## Exemple de Quiz
- [Quiz de démonstration](https://1000i100.frama.io/quiz-maker/demo-examples/quiz.html) et le [fichier source](https://framagit.org/1000i100/quiz-maker/raw/main/demo-examples/quiz.txt) dont est issue cette démo.
- [Quiz de démonstration : agression-fuite-inibition](https://1000i100.frama.io/quiz-maker/demo-examples/agression-fuite-inibition.html) et le [fichier source](https://framagit.org/1000i100/quiz-maker/-/blob/main/demo-examples/agression-fuite-inibition.quiz) dont est issue cette démo.

<iframe src="https://1000i100.frama.io/quiz_maker/demo-examples/" width="100%" height="500px"></iframe>

## Usage en ligne de commande :
```
npx quiz-maker srcFileOrFolder targetFolder
```
