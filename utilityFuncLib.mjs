export function scoreName(obj) {
	const scoreName = {};
	Object.keys(obj.options).filter(o=>o===o.toUpperCase()).forEach(o=>{
		const scoreInfo = scoreType2nameAndPosition(obj.options[o]);
		if(scoreInfo.name) scoreName[scoreInfo.name] = scoreInfo.position;
	});
	return scoreName;
}
export function scoreType2nameAndPosition(scoreType) {
	const tmp = scoreType.split(' ');
	const position = tmp.shift().split('-').map(str=>parseFloat(str));
	const name = tmp.join(' ');
	return {name,position};
}
export function questionCount(obj) { return obj.questions.length; }
export function scoreSum(score) { return score.reduce((acc,cur)=>acc+=cur,0); }
export function scoreStats(quoted,questionCount) {
	return {
		questionCount:questionCount,
		answeredCount:quoted.answered,
		scoreSum:quoted.score.reduce((acc,cur)=>acc+=cur,0)
	};
}
export function getUrlJson() { return JSON.parse(decodeURIComponent(location.hash).substr(1)); }
export function scoreQuotation(quotation,options){
	const score = [];
	Object.keys(quotation).forEach(scoreType=>{
		if(options[scoreType]){
			const scoreInfo = scoreType2nameAndPosition(options[scoreType]);
			scoreInfo.position.forEach((p,i)=>score[i]?score[i]+=quotation[scoreType]*p:score[i]=quotation[scoreType]*p)
		}
	});
	let scale = options.resultScaleFunc?Math[options.resultScaleFunc]:(x)=>x;
	return score.map(scale);
}
export function shuffle(a) {
	for (let i = a.length - 1; i > 0; i--) {
		const j = Math.floor(Math.random() * (i + 1));
		[a[i], a[j]] = [a[j], a[i]];
	}
	return a;
}
