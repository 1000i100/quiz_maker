```yaml
min: 1 # AnswserByQuestion
max: 2 # AnswserByQuestion

random: true

goBackButton: true
homeLink: https://framagit.org/1000i100/quiz_maker # for result screen
restartButton: true # for result screen

quotation: public # unsafe quotation freely available inside public quiz page

#resultScaleFunc: exp

toTopMedian: show
toRightMedian: show
resultView: trikala
subArea:
- position: [0.2,1,0.2]
  titlePosition: [0.2,1,0.2]
  title: Zone haute
  url: http://perdu.com
  description: |-
    Bla bla bla en Markdown
- {position: [1,1,0.1], title: Haut Gauche}
- {position: [0.1,1,1], title: Haut Droit}
- {position: [1,1,1], title: Centre}
- {position: [1,0.2,0.2], title: Bas Gauche}
- {position: [0.2,0.2,1], title: Bas Droit}
- {position: [1,0.1,1], title: Bas}


A: 1-0-0 Agression  # 1-0-0 = Bottom Left for text position in trikala and for quotation pound
F: 0-0-1 Fuite      # 0-0-1 = Bottom Right
I: 0-1-0 Innibition # 0-1-0 = Top Middle
```
```css

* {color:red !important;}

/* qqa : question, [quotation] answer */
```
```qqa
Cas de figure 1

[A] A l'assault
[F] Tayo tayo
[I] Je suis un arbre
[A] Intimidation
[F] fuite
[I] Innibition

Cas de figure 2

[A] A l'assault
[F] Tayo tayo
[I] Je suis un arbre
[A] Intimidation
[F] fuite
[I] Innibition

Cas de figure 3

[A] A l'assault
[F] Tayo tayo
[I] Je suis un arbre
[A] Intimidation
[F] fuite
[I] Innibition

Cas de figure 4

[A] A l'assault
[F] Tayo tayo
[I] Je suis un arbre
[A] Intimidation
[F] fuite
[I] Innibition

Cas de figure 5

[A] A l'assault
[F] Tayo tayo
[I] Je suis un arbre
[A] Intimidation
[F] fuite
[I] Innibition

Cas de figure 6

[A] A l'assault
[F] Tayo tayo
[I] Je suis un arbre
[A] Intimidation
[F] fuite
[I] Innibition

Cas de figure 7

[A] A l'assault
[F] Tayo tayo
[I] Je suis un arbre
[A] Intimidation
[F] fuite
[I] Innibition

Cas de figure 8

[A] A l'assault
[F] Tayo tayo
[I] Je suis un arbre
[A] Intimidation
[F] fuite
[I] Innibition

Cas de figure 9

[A] A l'assault
[F] Tayo tayo
[I] Je suis un arbre
[A] Intimidation
[F] fuite
[I] Innibition

Cas de figure 10

[A] A l'assault
[F] Tayo tayo
[I] Je suis un arbre
[A] Intimidation
[F] fuite
[I] Innibition
```
