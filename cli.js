#!/usr/bin/env node
const quizMaker = require('./generated.quizMaker.js');
const fs = require('fs');
const programName = process.argv[1].split('/').pop();
let quizFilePath = process.argv[2];
if(process.argv.length !== 3){
	console.error(`\nUsage : ${programName} quizFile`);
	process.exit(1);
}
const quizFileName = quizFilePath.split('/').pop().split('.').shift();
const outputfilesStrings = quizMaker(fs.readFileSync(quizFilePath,"utf8"),quizFileName);
Object.keys(outputfilesStrings).forEach((fileName)=>{
	const pathParts = fileName.split('/');
	pathParts.pop();
	if(pathParts.length>0) fs.mkdirSync(pathParts.join('/'),{recursive: true});
	fs.writeFileSync(fileName, outputfilesStrings[fileName])
});
