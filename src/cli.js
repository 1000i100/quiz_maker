#!/usr/bin/env node
const quizMaker = require('./quizMaker.cjs');
const fs = require('fs');
if(process.argv.length < 4) usageError('not enough arguments');
if(process.argv.length > 4) usageError('too many arguments');

let quizSrcFilePath = process.argv[2];
let targetPath = process.argv[3];

if(fs.lstatSync(quizSrcFilePath).isFile()) processOneQuiz(quizSrcFilePath,targetPath);
else if(fs.lstatSync(quizSrcFilePath).isDirectory()){
  const srcPath = trimLastSlash(quizSrcFilePath);

  fs.readdirSync(srcPath).forEach((srcFile)=>processOneQuiz(`${srcPath}/${srcFile}`,targetPath));
} else usageError(`${quizSrcFilePath} is not found as file or directory.`);

function processOneQuiz(src,targetPath){
  targetPath = trimLastSlash(targetPath);
  const quizFileName = src.split('/').pop().split('.').shift();
  const outputFilesStrings = quizMaker(fs.readFileSync(src,"utf8"),quizFileName);
  Object.keys(outputFilesStrings).forEach((fileName)=>{
    const pathParts = [...targetPath.split('/'), ...fileName.split('/')] ;
    pathParts.pop();
    if(pathParts.length>0) fs.mkdirSync(pathParts.join('/'),{recursive: true});
    fs.writeFileSync(`${targetPath}/${fileName}`, outputFilesStrings[fileName])
  });

}
function usageError(customMessage){
  const programName = process.argv[1].split('/').pop();
  if(customMessage) console.error(`\nError : ${customMessage}`);
  console.error(`\nUsage : ${programName} quizSrcFileOrFolder targetFolder`);
  process.exit(1);
}
function trimLastSlash(path){
  if(path[path.length-1] === '/') path = path.substr(0,path.length-1);
  return path;
}
