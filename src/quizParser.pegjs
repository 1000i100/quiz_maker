{
  function m(...objs){ //merge
    return objs.reduce(merge,{});
  }
	function clone(json) {
		return JSON.parse(JSON.stringify(json))
	}
  function merge(acc, obj) {
    if(typeof acc !== "object") return clone(obj);
    if(!obj) return acc;
    if(obj.class && acc.class){
      let classes = {}
      acc.class.split(" ").forEach(c=>classes[c]=c);
      obj.class.split(" ").forEach(c=>classes[c]=c);
      obj.class = Object.keys(classes).join(' ');
    } ;
    let res = clone(acc);
    for (let key in obj) {
      if (typeof obj[key] === "object" && typeof res[key] !== 'undefined') {
        res[key] = merge(res[key], obj[key]);
      }
      else res[key] = obj[key];
    }
    return res;
  }
  function t(str){
    try{
        return JSON.parse(`{"val":${str}}`).val;
    } catch (e){
    return str.trim();
    }
  }
}


GlobalStructure
  = oa:OptionsArea ConfigSeparator fc:FormContent { return m({options:oa},fc); }
  / Separator
  / FormContent

OptionsArea
  = o:Option+ { return m(...o); }

Option
  = Space key:Id Space ":" Space value:Text Space Comment? Separator { return {[key]:t(value)}; }
  / Space Comment? Separator {return {}; }
NextOption = o:Option { return o; }
Options = no:NextOption* { return m(...no); }




ConfigSeparator = "--" "-"+ Separator
FormContent = qa:QA* { return {questions: qa} }
QA = q:Question a:Answer+ {return {label:q,answers:a} }
Question = txt:Text Comment? Separator { return txt.trim(); }
Answer = "[" quotation:Id "]" Space label:Text Comment? Separator? { return {label:label,quotation:quotation}; }

Text = $([^/[\n\r]+ / ("/" [^/[\n\r]+))+
Id = $[_0-9a-zA-Z]+
Space = [ \t]* BlockComment? [ \t]*
Separator = Space [\n\r]+ Space
Comment
  = Space BlockComment Space
  / Space "//" [^\n\r]*

BlockComment = "/*" ([^*]+/[*][^/])* "*/"
