export function scoreName(obj) {
	const scoreName = {};
	Object.keys(obj.options).filter(o=>o===o.toUpperCase()).forEach(o=>{
		const scoreInfo = scoreType2nameAndPosition(obj.options[o]);
		if(scoreInfo.name) scoreName[scoreInfo.name] = scoreInfo.position;
	});
	return scoreName;
}
export function scoreType2nameAndPosition(scoreType) {
	const tmp = scoreType.split(' ');
	const position = tmp.shift().split('-').map(str=>parseFloat(str));
	const name = tmp.join(' ');
	return {name,position};
}
export function questionCount(obj) { return obj.questions.length; }
export function scoreSum(score) { return score.reduce((acc,cur)=>acc+=cur,0); }
export function scoreStats(quoted,questionCount) {
	return {
		questionCount:questionCount,
		answeredCount:quoted.answered,
		scoreSum:quoted.score.reduce((acc,cur)=>acc+=cur,0)
	};
}
export function getUrlJson() { return JSON.parse(decodeURIComponent(location.hash).substr(1)); }
export function scoreQuotation(quotation,options){
	const score = [];
	Object.keys(quotation).forEach(scoreType=>{
		if(options[scoreType]){
			const scoreInfo = scoreType2nameAndPosition(options[scoreType]);
			scoreInfo.position.forEach((p,i)=>score[i]?score[i]+=quotation[scoreType]*p:score[i]=quotation[scoreType]*p)
		}
	});
	let scale = options.resultScaleFunc?Math[options.resultScaleFunc]:(x)=>x;
	return score.map(scale);
}
export function shuffle(a) {
	for (let i = a.length - 1; i > 0; i--) {
		const j = Math.floor(Math.random() * (i + 1));
		[a[i], a[j]] = [a[j], a[i]];
	}
	return a;
}
/**
 * object / array manipulation
 */
export function merge(...objs){
  return objs.reduce(merge2, {});
}
export function clone(json) {
  return JSON.parse(JSON.stringify(json))
}
export function merge2(acc, obj) {
  if(typeof acc !== "object") return clone(obj);
  if(!obj) return acc;
  if(obj.class && acc.class){
    let classes = {};
    acc.class.split(" ").forEach(c=>classes[c]=c);
    obj.class.split(" ").forEach(c=>classes[c]=c);
    obj.class = Object.keys(classes).join(' ');
  }
  let res = clone(acc);
  for (let key in obj) if(obj.hasOwnProperty(key)){
    if (typeof obj[key] === "object" && typeof res[key] !== 'undefined') {
      res[key] = merge2(res[key], obj[key]);
    }
    else res[key] = obj[key];
  }
  return res;
}
