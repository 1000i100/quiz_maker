export default function(obj) {
	let footerLeftButtons='';
	if(obj.options.homeLink) footerLeftButtons+=`<a class="button" href="${obj.options.homeLink}">⌂</a>`;
	if(obj.options.restartButton) footerLeftButtons+=`<a class="button back" href="">⟳</a>`;
	return `
<script>
function renderResult(quotation){
	
	document.body.innerHTML = \`
<section id="result">
	<div class="page">
		<h1>Generic result viewer</h1>
		<label>questionCount : ${obj.questions.length}</label>
		<label>\${Object.keys(quotation).map(k=>k+' : '+JSON.stringify(quotation[k])).join('</label><label>')}</label>
		<footer>
			${footerLeftButtons}
			<div class="errors">use option <code>resultView: aMoreAccurateResultRenderer</code> in your quiz file for a better view.</div>
			<div>&nbsp;</div>
		</footer>
	</div>
</section>
\`;
}
</script>
`;
}

