const fs = require("fs");
const ejs = require('ejs');

let str = `const resultViewers = {};
`;
function splitNameExt(fileName){
	const pPart = fileName.split('/');
	const fPart = pPart.pop().split('.');
	let path = pPart.join('/');
	if(path) path+='/';
	let gen=fPart[0]==='generated'?fPart.shift()+'.':'';
	let ext = '.'+fPart.pop();
	let name = fPart.join('.');
	return {path,gen,name,ext}
}
fs.readdirSync('resultViewer').forEach((fileName)=>{
	const filePath = `./resultViewer/${fileName}`;
	let f = splitNameExt(filePath);
	if(f.gen) return;
	if(f.ext==='.html') return;
	if(f.ext==='.ejs'){
		f = splitNameExt(f.path+f.name);
		f.gen = 'generated.';
		fs.writeFileSync(f.path+f.gen+f.name+f.ext, ejs.render(fs.readFileSync(filePath, 'utf8'),{},{filename:filePath}));
	}
	str+=`
import ${f.name==='default'?'defaultView':f.name} from "${f.path+f.gen+f.name+f.ext}";
resultViewers['${f.name}'] = ${f.name==='default'?'defaultView':f.name};
`;
});
str+=`
export default resultViewers;
`;

fs.writeFileSync(`generated.importAll.mjs`, str);
